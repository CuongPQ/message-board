/**
* MB_Validate_Exist_Confirmation_Tri.
*
* @author: CuongPQ.
* @date: 10 Aug 2015.
* @description: Validate message confirm before insert into DB.
* if current users has confirm these messages, so they can not confirm again.
*
*/
trigger MB_Validate_Exist_Confirmation_Tri on Message_Confirmation__c (Before Insert) {
    
    //create the set to store the list unique user Id.
    Set<String> setUserId = new Set<String>();
    
    //Create the set to store the list unique Bulletin Board Id.
    Set<String> setBulletinBoardId = new Set<String>();
    
    //Loop on list new record preparing to import to DB.
    //add all user it into set user id.
    //add all bulletin board id into set bulletin board id.
    for (Message_Confirmation__c item : Trigger.New)
    {
        setUserId.add(item.User__c);
        setBulletinBoardId.add(item.Bulletin_Board__c);
    }
    
    //make a query to get all confirm message of these users and these message confirm record.
    //note: ONLY check these valid message (these message have range posting and end posted date still valid with today.
    List<Message_Confirmation__c > listDataChecking = [SELECT Bulletin_Board__c, User__c, Review_Time__c FROM Message_Confirmation__c  WHERE Bulletin_Board__c IN :setBulletinBoardId AND User__c IN :setUserId 
    AND Bulletin_Board__r.Posting_Date__c <= TODAY AND Bulletin_Board__r.Posted_End_Date__c >= TODAY];
    
    //make loop to check all preparing record again.
    for (Message_Confirmation__c item : Trigger.New)
    {
        //loop all list message confirm that queried above.
        for (Message_Confirmation__c itemCheck : listDataChecking)
        {
            //if a message confirm record that confirmed by user, so add the error to alert user know they can not confirm again.
            // and continue check another message.
            if (item.User__c == itemCheck.User__c && item.Bulletin_Board__c == itemCheck.Bulletin_Board__c)
            {
                item.adderror(Label.MB_Error_Confirmation_Message_Exist);
                break;
            }
        }
    }
}