/**
* MB_MessageList_Ctr_Test
*
* @author: CuongPQ.
* @date: 11 Aug 2015.
* @description: Test MB_MessageList_Ctr.
*
*/
@isTest
public class MB_MessageList_Ctr_Test
{    
    @testSetup static void setup() {
        // Create common test bulletin boards.
        List<Bulletin_Board__c> testBulletinBoard = new List<Bulletin_Board__c>();
        for(Integer i = 0 ;i < 25 ; i++) {
            Bulletin_Board__c item = new Bulletin_Board__c();
            item.Title__c = '日本は処理があります'  + i;
            item.Comment__c = '日本は●●の日です。<br>16時までに、システムへの登録を完了させてください。';
            item.Posting_Date__c = Date.today();
            item.Posted_End_Date__c= Date.today() + 30;
            testBulletinBoard.add(item);
        }
        insert testBulletinBoard ;
              
    }
    
    @isTest static void testInit_MB_MessageList_Ctr() {
        
        //create the controller.
        MB_MessageList_Ctr mesCtr = new MB_MessageList_Ctr ();
        System.assertEquals(mesCtr.numberDisplayData, mesCtr.listMessage.size());
        //System.assertEquals(2 , mesCtr.getNumberPage());
        System.assertEquals(25 , mesCtr.allMessageRecord);

        //create the user session filter.
        User_Session_Filter__c userSessionFilter = new User_Session_Filter__c();
        userSessionFilter.Last_Filter_Field__c = 'Posting_Date__c';
        userSessionFilter.Page_Name__c= MB_MessageList_Ctr.CONST_PAGE_NAME;
        userSessionFilter.Is_Ascending__c = true;
        userSessionFilter.Confirmation_Status__c = MB_MessageList_Ctr.CONST_SELECT_ALL;
        insert userSessionFilter;
        
        try
        {
            //try to insert duplicate user session filter data.
            User_Session_Filter__c userSessionFilterNew = new User_Session_Filter__c();
            userSessionFilterNew.Last_Filter_Field__c = 'Posting_Date__c';
            userSessionFilterNew.Page_Name__c= MB_MessageList_Ctr.CONST_PAGE_NAME;
            userSessionFilterNew.Is_Ascending__c = true;
            userSessionFilterNew.Confirmation_Status__c = MB_MessageList_Ctr.CONST_SELECT_ALL;
            insert userSessionFilterNew;
        }
        catch (Exception e)
        {
            System.assertEquals(true , e.getMessage() != null);
        }
        
        //re-create the controller.
        mesCtr = new MB_MessageList_Ctr (); 
        System.assertEquals(true , mesCtr.userFilterData != null);
    }
    
    @isTest static void testNextAction() {
        
        //create the controller.
        MB_MessageList_Ctr mesCtr = new MB_MessageList_Ctr (); 
        Integer currenPage = mesCtr.currentPageNum;
        
        //make next page action.
        mesCtr.nextPageAction();
        System.assertEquals(true , mesCtr.currentPageNum > currenPage );
        
    }
    
    @isTest static void testPreviousAction() {
        
        //create the controller.
        MB_MessageList_Ctr mesCtr = new MB_MessageList_Ctr ();
        
        //make next page action.
        mesCtr.nextPageAction();
        //keep page number after next page.
        Integer currenPage = mesCtr.currentPageNum;
        //make previous page action.
        mesCtr.previousPageAction();
        System.assertEquals(true , mesCtr.currentPageNum < currenPage );
    }
    
    @isTest static void testIntSelectListOptions() {
    
        //create the controller.
        MB_MessageList_Ctr mesCtr = new MB_MessageList_Ctr ();
        System.assertEquals(3, mesCtr.getSelectionOptionDataList().size() );
        
    }
    
    @isTest static void testConfirmMessage() {
        
        //initiate the VF MB_MessageList_Page page for current test.
        PageReference pageRef = Page.MB_MessageList_Page;
        Test.setcurrentPage(pageref);
        
        //create the controller.
        MB_MessageList_Ctr mesCtr = new MB_MessageList_Ctr ();
        
        //put the parameter into page test.
        pageref.getParameters().put(MB_MessageList_Ctr.CONST_BULLETIN_BOARD_ID_PARAM , mesCtr.listMessage[0].Id);
        mesCtr.confirmMessage();
        System.assertEquals(false , mesCtr.mapButtonStatus.get(mesCtr.listMessage[0].Id));
    }
    
    @isTest static void testConfirmMessageError() {
        
        //initiate the VF MB_MessageList_Page page for current test.
        PageReference pageRef = Page.MB_MessageList_Page;
        Test.setcurrentPage(pageref);
        
        //get current page message. expect it empty.
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(0, pageMessages.size());
        
        //create the controller.
        MB_MessageList_Ctr mesCtr = new MB_MessageList_Ctr ();
        
        //put the parameter into page test.
        Apexpages.currentPage().getParameters().put(MB_MessageList_Ctr.CONST_BULLETIN_BOARD_ID_PARAM , mesCtr.listMessage[0].Id);
        
        //make action confirm message.
        mesCtr.confirmMessage();
        
        //make action confirm message again to make sure to have an error.
        mesCtr.confirmMessage();
        Boolean messageFound = false;
        pageMessages = ApexPages.getMessages();
        for(ApexPages.Message message : pageMessages) {
            if(message.getSeverity() == ApexPages.Severity.ERROR && message.getDetail().contains(Label.MB_Error_Confirmation_Message_Exist))
            {
                messageFound = true;        
            }
        }
        System.assertEquals(true, messageFound );
        
    }
    
    @isTest static void testFilterHeaderColumn() {
        
        //initiate the VF MB_MessageList_Page page for current test.
        PageReference pageRef = Page.MB_MessageList_Page;
        Test.setcurrentPage(pageref);
        
        //create the controller.
        MB_MessageList_Ctr mesCtr = new MB_MessageList_Ctr ();
        
        //put the parameter into page test.
        Apexpages.currentPage().getParameters().put(MB_MessageList_Ctr.CONST_FILTER_PARAM, mesCtr.listMessage[0].Id);
        
        System.assertEquals(true, mesCtr.userFilterData.Is_Ascending__c );
        System.assertEquals(null , mesCtr.userFilterData.Id );
        
        //make action filter data.
        mesCtr.filter();
        System.assertEquals(true,  mesCtr.userFilterData.Id != null);
        
        //make action filter data again on the same field name to make sure the order is opposite.
        mesCtr.filter();
        System.assertEquals(false,  mesCtr.userFilterData.Is_Ascending__c );
    }
    
    @isTest static void testFilterStatus() {
    
         //initiate the VF MB_MessageList_Page page for current test.
        PageReference pageRef = Page.MB_MessageList_Page;
        Test.setcurrentPage(pageref);
        
        //create the controller.
        MB_MessageList_Ctr mesCtr = new MB_MessageList_Ctr ();
        Apexpages.currentPage().getParameters().put(MB_MessageList_Ctr.CONST_BULLETIN_BOARD_ID_PARAM, mesCtr.listMessage[0].Id);
        
        //set up and change status filter to not confirmation.
        mesCtr.selectBoxData  = MB_MessageList_Ctr.CONST_SELECT_NOT_CONFIRMATION ;
        mesCtr.changeStatusFilter();
        System.assertEquals(mesCtr.numberDisplayData,  mesCtr.listMessage.size());
        
        //make action confirm current message.
        mesCtr.confirmMessage();
        //set up and change status filter to confirmation.
        mesCtr.selectBoxData  = MB_MessageList_Ctr.CONST_SELECT_CONFIRMATION ;
        mesCtr.changeStatusFilter();
        System.assertEquals(1,  mesCtr.listMessage.size());
    }
    
     @isTest static void testBuildListDataError() {
        
        //create the controller.
        MB_MessageList_Ctr mesCtr = new MB_MessageList_Ctr ();
        System.assertEquals(null, mesCtr.buildQueryData(0,'test', true));
     }
}