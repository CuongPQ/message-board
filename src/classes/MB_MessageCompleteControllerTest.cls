/**
 * MB_MessageCompleteControllerTest
 *
 * @author Nguyen Hoai Nam
 * @date 08/12/2015
 *
 * @description test for MB_MessageCompleteController
 */
 @isTest
 private class MB_MessageCompleteControllerTest {
     static testMethod void testMethodInit1() {
        // コントローラ
        MB_MessageCompleteController con = new MB_MessageCompleteController();
        // メッセージ確認履歴のレコードを作成
        getMessageConfirm();
        //
        Bulletin_Board__c bulletinBoardInfo = [SELECT Id FROM Bulletin_Board__c WHERE Title__c = 'NamTest0'];
        System.Debug(bulletinBoardInfo);
        Pagereference processPage = Page.MB_MessageComplete;
        processPage.getParameters().put('aId', bulletinBoardInfo.Id);
        // テスト実行
        Test.startTest();
        Test.setCurrentPage(processPage);
        con.init();
        Test.stopTest();
        // メッセージ確認履歴リスト
        List<Message_Confirmation__c> lstMessageConfirmation = [SELECT Id FROM Message_Confirmation__c];
        // 結果確認
        System.assertEquals(2, lstMessageConfirmation.size());
    }
    static testMethod void testMethodInit2() {
        // コントローラ
        MB_MessageCompleteController con = new MB_MessageCompleteController();
        // メッセージ確認履歴のレコードを作成
        getMessageConfirm();
        //
        Bulletin_Board__c bulletinBoardInfo = [SELECT Id FROM Bulletin_Board__c WHERE Title__c = 'NamTest1'];
        //System.Debug(bulletinBoardInfo);
        Pagereference processPage = Page.MB_MessageComplete;
        processPage.getParameters().put('aId', bulletinBoardInfo.Id);
        // テスト実行
        Test.startTest();
        Test.setCurrentPage(processPage);
        con.init();
        Test.stopTest();
        // メッセージ確認履歴リスト
        List<Message_Confirmation__c> lstMessageConfirmation = [SELECT Id FROM Message_Confirmation__c];
        // 結果確認
        System.assertEquals(1, lstMessageConfirmation.size());
    }
    static testMethod void testMethodReturnToPreviousPage() {
        // コントローラ
        MB_MessageCompleteController con = new MB_MessageCompleteController();
        // メッセージ確認履歴のレコードを作成
        getMessageConfirm();
        //
        Bulletin_Board__c bulletinBoardInfo = [SELECT Id FROM Bulletin_Board__c WHERE Title__c = 'NamTest1'];
        Pagereference currentPage = Page.MB_MessageComplete;
        currentPage.getParameters().put('aId', bulletinBoardInfo.Id);
        Pagereference processPage = new Pagereference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + bulletinBoardInfo.Id);
        // テスト実行
        Test.startTest();
        Test.setCurrentPage(currentPage);
        con.init();
        Pagereference returnPage = con.returnToPreviousPage();
        Test.stopTest();
        // 結果確認
        System.assertEquals(processPage.getUrl(), returnPage.getUrl());
    }
    static void getBulletinBoard(){
        // 掲示板リスト
        List<Bulletin_Board__c> lstBulletinBoard = new List<Bulletin_Board__c>();
        for(Integer i = 0; i < 2; i++) {
            Bulletin_Board__c item = new Bulletin_Board__c();
            item.Title__c = 'NamTest'  + i;
            item.Comment__c = 'NamTest';
            item.Posting_Date__c = Date.today() - 30;
            item.Posted_End_Date__c = Date.today() + 30;
            lstBulletinBoard.add(item);
        }
        insert lstBulletinBoard;
    }
    static void getMessageConfirm(){
        // 掲示板リスト
        getBulletinBoard();
        // メッセージ確認履歴
        Bulletin_Board__c bulletinBoardInfo = [SELECT Id FROM Bulletin_Board__c WHERE Title__c = 'NamTest1'];
        Message_Confirmation__c item = new Message_Confirmation__c();
        item.User__c = UserInfo.getUserId();
        item.Bulletin_Board__c = bulletinBoardInfo.Id;
        item.Review_Time__c = DateTime.now();
        insert item;
    }
 }