/**
 * MB_MessageCompleteController
 *
 * @author Nguyen Hoai Nam
 * @date 08/07/2015
 *
 * @description handle complete message
 */
public with sharing class MB_MessageCompleteController {
    // id of message board
    private string bulletinBoardId = '';
    // status confirm message board
    public boolean isConfirm {get;set;}
    // status error when date confirm message not valid
    public boolean isDateError {get;set;}
    public MB_MessageCompleteController(){
        // constructor
    }
    
    /**
     *
     * @description Check the current message board is confirmed or not. If confirmed, alert the message. If not, make confirm and alert message. 
     * @param Nothing
     * @return Nothing
     */
    public void init(){
        // message board not yet confirm
        isConfirm = false;
        isDateError = false;
        // get message board id from URL
        bulletinBoardId = System.currentPageReference().getParameters().get('aId');
        // get message confirm information
        List<Message_Confirmation__c> listMessageConfirm = [SELECT Id 
                                                            FROM Message_Confirmation__c 
                                                            WHERE User__c =: UserInfo.getUserId() 
                                                            AND Bulletin_Board__c =: bulletinBoardId];
        // if message board not yet confirm, create new message confirm 
        if(listMessageConfirm.size() > 0)
        {
            isConfirm = true;
        }
        else
        {
            try
            {
                Message_Confirmation__c item = new Message_Confirmation__c();
                item.User__c = UserInfo.getUserId();
                item.Bulletin_Board__c = bulletinBoardId;
                item.Review_Time__c = DateTime.now();
                insert item;
            }
            catch(Exception e)
            {
                isDateError = true;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                ApexPages.addMessage(msg);
            }
        } 
    }
    
    /**
     *
     * @description return the bulletin board (message board) detail page.
     * @param Nothing
     * @return pageReference 
     */
    public pageReference returnToPreviousPage(){
        PageReference page = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + bulletinBoardId );
        page.setRedirect(true);
        return page;
    }
}