/**
* MB_MessageList_Ctr
*
* @author: CuongPQ.
* @date : 06 Aug 2015.
* @description: controlller for page message list.
*
*/
public with sharing class MB_MessageList_Ctr
{
    //---------- Define Constant Variable ---------------------------
    //public static integer CONST_MAX_ITEM = 20;
    public static string CONST_FILTER_PARAM = 'filter_parameter';
    public static string CONST_BULLETIN_BOARD_ID_PARAM = 'bulletinId';
    public static string CONST_SELECT_ALL = 'すべて';//'All';
    public static string CONST_SELECT_CONFIRMATION = '確認済み';//'Confirmation';
    public static string CONST_SELECT_NOT_CONFIRMATION = '未確認';//'Not_Confirmation';
    public static string CONST_PAGE_NAME = 'MB_MessageList_Page';
    public static integer CONST_MAX_SIZE = 10000;
    //------------------- End defin contant variable ----------------
    
    //store the number bulletin board record.
    public integer allMessageRecord {get;set;} 
    
    //Store the field filter by User.
    public User_Session_Filter__c userFilterData {get;set;}    
    
    //Store the list bulletin board and confirm message.
    public List<Bulletin_Board__c> listMessage {get;set;}
    
    //Store the status of button confirm of these item on list message.
    public Map<String,Boolean> mapButtonStatus {get;set;}
    
    //Store the current page number.
    public integer currentPageNum {get;set;}
    
    //Store select option data.
    public string selectBoxData {get;set;}
    
    //store the query string.
    public string queryData {get;set;}
    
    //store the number display.
    public Integer numberDisplayData{set;
        get
        {
            return convertLabelIntoInteger(Label.MB_DEFAULT_MESSAGE_ITEM);
        }
    }
    
    public Integer maxDataItems{set;
        get
        {
            return convertLabelIntoInteger(Label.MB_MAXIMUM_MESSAGE_ITEM);            
        }
    }

    /**
    * @contructor for extend controller.
    * @description: initation. 
    * - query user session filter.
    * - set first page num.
    * - set filter status message confirmation is all.
    * - build data list.
    */
    public MB_MessageList_Ctr(ApexPages.StandardController stdController)
    {
        initData();
    }
    
    /**
    * @create the variable inherit standard controller.
    * make the query by query locator of standard controller.
    * put the current page size and put the current page number into it.
    * the result will be out is list data of object (maximimum 10000 records). 
    */
    public ApexPages.StandardSetController setCon {
        get {
            String newQuery = this.queryData.subString(0,this.queryData.indexOf('LIMIT'));
            system.debug('the new query is: ' + newQuery);
            //if(setCon == null) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                     newQuery ));
            //}
            setCon.setPageSize(numberDisplayData);
            setCon.setpageNumber(this.currentPageNum);
            
            return setCon;
        }
        set;
    }   
    
    /**
    * @contructor for controller.
    * @description: initation. 
    * - query user session filter.
    * - set first page num.
    * - set filter status message confirmation is all.
    * - build data list.
    */
    public MB_MessageList_Ctr()
    {
        initData();
    }
    
    /**
    * --initData--.
    * @author: CuongPQ
    * @description: initation. 
    * - query user session filter.
    * - set first page num.
    * - set filter status message confirmation is all.
    * - build data list.
    * @parameter: none.
    * @return: none.
    */
    public void initData()
    {
        // make query user filter obj.
        List<User_Session_Filter__c > listUserFilterData = [SELECT Id, Last_Filter_Field__c, OwnerId, Is_Ascending__c, Confirmation_Status__c FROM User_Session_Filter__c WHERE OwnerId =: UserInfo.getUserId() AND Page_Name__c=:CONST_PAGE_NAME];
        if (listUserFilterData .size() > 0)
        {
            this.userFilterData = listUserFilterData[0];
        }
        
        this.queryData = '';

        if (this.userFilterData == null)
        {
            this.userFilterData = new User_Session_Filter__c ();
            this.userFilterData.Page_Name__c = CONST_PAGE_NAME;
            this.userFilterData.Last_Filter_Field__c = 'Posting_Date__c';
            this.userFilterData.Is_Ascending__c = true;
            this.userFilterData.Confirmation_Status__c = CONST_SELECT_ALL;
        }
        this.currentPageNum = 1;
        this.selectBoxData = this.userFilterData.Confirmation_Status__c;
        this.listMessage = buildQueryData(0,this.userFilterData.Last_Filter_Field__c, this.userFilterData.Is_Ascending__c);
    }
    
    /**
    *  @ author: CuongPQ.
    *  @ date: 13 Aug 2015.
    *  @ description: Handle action store the user session filter.
    */
    public void upsertUserSessionFilter()
    {
        if (this.userFilterData.Id == null)
        {
            system.debug('userFilterData: ' + userFilterData);
            insert this.userFilterData;
        }
        else
        {
            update this.userFilterData;
        }
    }
    
    /**
    *  @ author: CuongPQ.
    *  @ date: 10 Aug 2015.
    *  @ description: Handle action pick the value of select box.
    */
    public void changeStatusFilter()
    {
        this.currentPageNum = 1;
        this.userFilterData.Confirmation_Status__c = this.selectBoxData ;
        upsertUserSessionFilter();
        this.listMessage = buildQueryData(0,this.userFilterData.Last_Filter_Field__c, this.userFilterData.Is_Ascending__c);
    }
    
    /**
    *  @ author: CuongPQ.
    *  @ date: 10 Aug 2015.
    *  @ description: build the selection data list.
    */
    public List<SelectOption> getSelectionOptionDataList()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(CONST_SELECT_ALL , Label.MB_Label_All));
        options.add(new SelectOption(CONST_SELECT_CONFIRMATION , Label.MB_Label_Confirmation));
        options.add(new SelectOption(CONST_SELECT_NOT_CONFIRMATION , Label.MB_Label_Not_Comfirmation));
        return options;
    }
    
    /**
    *  @ author: CuongPQ.
    *  @ date: 07 Aug 2015.
    *  @ description: Handle Action Next Page.
    */
    public void nextPageAction()
    {
        system.debug('currentPageNum = ' + this.currentPageNum );
        system.debug('getNumberPage = ' + getNumberPage() );
        if (this.currentPageNum < getNumberPage())
        {
            this.currentPageNum ++;
            this.listMessage = buildQueryData((this.currentPageNum - 1) * numberDisplayData ,this.userFilterData.Last_Filter_Field__c, this.userFilterData.Is_Ascending__c);
        }
    }
    
    /**
    *  @ author: CuongPQ.
    *  @ date: 07 Aug 2015.
    *  @ description: Handle Action Previous Page.
    */
    public void previousPageAction()
    {
        if (this.currentPageNum >1)
        {
            this.currentPageNum --;
            this.listMessage = buildQueryData((this.currentPageNum -1) * numberDisplayData ,this.userFilterData.Last_Filter_Field__c, this.userFilterData.Is_Ascending__c);
        }
    }
    
    /**
    *  @ author: CuongPQ.
    *  @ date: 06 Aug 2015.
    *  @ description: Get the Number Page valid.
    */
    public integer getNumberPage()
    {
        return ( Math.mod(this.allMessageRecord, numberDisplayData) != 0 ? this.allMessageRecord/numberDisplayData +1 : this.allMessageRecord/numberDisplayData);
    }
    
    /**
    *  @ author: CuongPQ.
    *  @ date: 06 Aug 2015.
    *  @ description: Handle action confirm message by user.
    */
    public void confirmMessage()
    {
        String bulletinBoardId = ApexPages.currentPage().getParameters().get(CONST_BULLETIN_BOARD_ID_PARAM);
        Boolean isValid = (bulletinBoardId InstanceOf ID) ? true : false ;
        if (isValid)
        {
            try
            {
                Message_Confirmation__c item = new Message_Confirmation__c();
                item.User__c = UserInfo.getUserId();
                item.Bulletin_Board__c = bulletinBoardId;
                item.Review_Time__c = DateTime.now();
                insert item;
                mapButtonStatus.put(bulletinBoardId, false);
            }
            catch( Exception e)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                ApexPages.addMessage(msg);
            }
        }
    }

    /**
    *  @ author: CuongPQ.
    *  @ date: 06 Aug 2015.
    *  @ description: handle action filter
    */
    public void filter()
    {
        String filterFieldName = ApexPages.currentPage().getParameters().get(CONST_FILTER_PARAM);
        system.debug('filterFieldName =' +filterFieldName );
        if (!String.isBlank(filterFieldName))
        {
            if (filterFieldName == this.userFilterData.Last_Filter_Field__c)
            {
                this.userFilterData.Is_Ascending__c = !this.userFilterData.Is_Ascending__c;
            }
            else
            {
                this.userFilterData.Last_Filter_Field__c = filterFieldName;
            }
            
            upsertUserSessionFilter();
            this.listMessage = buildQueryData((this.currentPageNum - 1) * numberDisplayData,this.userFilterData.Last_Filter_Field__c, this.userFilterData.Is_Ascending__c);
        }
    }
    
    /**
    *  @ author: CuongPQ.
    *  @ date: 06 Aug 2015.
    *  @ description: Map status of button confirm.
    */
    public void buildMapStatusButtonConfirm(List<Bulletin_Board__c> pListMessage)
    {
        this.mapButtonStatus = new Map<String,Boolean>();
        if (pListMessage != null)
        {
            for (Bulletin_Board__c item : pListMessage)
            {
                if (!this.mapButtonStatus.containsKey(item.Id))
                {
                    if (item.Bulletin_master__r.size() > 0)
                    {
                        this.mapButtonStatus.put(item.Id, false);
                    }
                    else
                    {
                        this.mapButtonStatus.put(item.Id, true);
                    }
                }
            }
        }
    }
    
    /**
    *  @ author: CuongPQ.
    *  @ date: 06 Aug 2015.
    *  @ description: Build & make Query data.
    *  @ param1 pOffset : the offset use for paging.
    *  @ param2 pOderBy: the field api use for order (sort).
    *  @ param3 pAscending: check accesending or desending (sort).
    *  @ return: List<Bulletin_Board__c> if having data. Null if having error or empty.
    */
    public List<Bulletin_Board__c> buildQueryData(Integer pOffset, String pOderBy, boolean pAscending)
    {
        try
        {
            if (numberDisplayData < 0 && maxDataItems < 0)
            {
                return this.listMessage;
            }
            String queryString = '';
            String queryCount = '';
            if (this.selectBoxData == CONST_SELECT_ALL )
            {
                queryString = 'SELECT Owner.Id, Owner.Name, Id, Comment__c, Title__c, Name, Summary_Content_Confirmmation__c, '
                + 'Posted_End_Date__c, Posting_Date__c, (SELECT Id, User__c, Review_Time__c FROM Bulletin_master__r WHERE User__c = \''
                + UserInfo.getUserId() + '\') FROM Bulletin_Board__c WHERE Posting_Date__c <= TODAY AND Posted_End_Date__c >= TODAY  ORDER BY '
                + pOderBy + (pAscending == true ? ' ASC' : ' DESC') + ' LIMIT ' + numberDisplayData + ' OFFSET ' +  pOffset ;
                
                queryCount = 'SELECT count(ID) counter FROM Bulletin_Board__c WHERE Posting_Date__c <= TODAY AND Posted_End_Date__c >= TODAY';
            }
            else if (this.selectBoxData == CONST_SELECT_CONFIRMATION )
            {
                queryString = 'SELECT Owner.Id, Owner.Name, Id, Comment__c, Title__c, Name, Summary_Content_Confirmmation__c, '
                + 'Posted_End_Date__c, Posting_Date__c, (SELECT Id, User__c, Review_Time__c FROM Bulletin_master__r WHERE User__c = \''
                + UserInfo.getUserId() + '\') FROM Bulletin_Board__c WHERE Posting_Date__c <= TODAY AND Posted_End_Date__c >= TODAY AND ID IN (SELECT Bulletin_Board__c FROM Message_Confirmation__c WHERE User__c = \''
                + UserInfo.getUserId() + '\' AND Bulletin_Board__r.Posting_Date__c <= TODAY AND Bulletin_Board__r.Posted_End_Date__c >= TODAY) ORDER BY '
                + pOderBy + (pAscending == true ? ' ASC' : ' DESC') + ' LIMIT ' + numberDisplayData + ' OFFSET ' +  pOffset ;
                
                queryCount = 'SELECT count(ID) counter FROM Bulletin_Board__c WHERE Posting_Date__c <= TODAY AND Posted_End_Date__c >= TODAY AND ID IN (SELECT Bulletin_Board__c FROM Message_Confirmation__c ' +
                'WHERE Bulletin_Board__r.Posting_Date__c <= TODAY AND Bulletin_Board__r.Posted_End_Date__c >= TODAY AND User__C =\'' + UserInfo.getUserId() +'\')';
            }
            else
            {
                queryString = 'SELECT Owner.Id, Owner.Name, Id, Comment__c, Title__c, Name, Summary_Content_Confirmmation__c, '
                + 'Posted_End_Date__c, Posting_Date__c, (SELECT Id, User__c, Review_Time__c FROM Bulletin_master__r WHERE User__c = \''
                + UserInfo.getUserId() + '\') FROM Bulletin_Board__c WHERE Posting_Date__c <= TODAY AND Posted_End_Date__c >= TODAY AND ID NOT IN (SELECT Bulletin_Board__c FROM Message_Confirmation__c WHERE User__c = \''
                + UserInfo.getUserId() + '\' AND Bulletin_Board__r.Posting_Date__c <= TODAY AND Bulletin_Board__r.Posted_End_Date__c >= TODAY) ORDER BY '
                + pOderBy + (pAscending == true ? ' ASC' : ' DESC') + ' LIMIT ' + numberDisplayData + ' OFFSET ' +  pOffset ;   
                
                queryCount = 'SELECT count(ID) counter FROM Bulletin_Board__c WHERE Posting_Date__c <= TODAY AND Posted_End_Date__c >= TODAY AND ID NOT IN (SELECT Bulletin_Board__c FROM Message_Confirmation__c ' +
                'WHERE Bulletin_Board__r.Posting_Date__c <= TODAY AND Bulletin_Board__r.Posted_End_Date__c >= TODAY AND User__C =\'' + UserInfo.getUserId() +'\')';             
            }
            System.debug('queryString =' + queryString );
            this.queryData = queryString;
            List<AggregateResult> resultCount = (List<AggregateResult>)database.query(queryCount);
            if (resultCount.size() > 0)
            {
                this.allMessageRecord = Integer.valueOf(resultCount[0].get('counter'));
            }
            List<Bulletin_Board__c> result = this.listMessage;
            
            //check the total result counter is over size (> 10000 is upon bound limitation).
            if (this.allMessageRecord < CONST_MAX_SIZE  && this.allMessageRecord < maxDataItems)
            {
                //use the query locator of Standard controller of Bulletin_Board__c to make the query that increase maximum number records upto 10.000 records.
                result = (List<Bulletin_Board__c>)setCon.getRecords();//(List<Bulletin_Board__c>)database.query(queryString);
                buildMapStatusButtonConfirm(result );
            }
            //check over 10000 records.
            else if (this.allMessageRecord >= CONST_MAX_SIZE)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, String.format(Label.MB_MAXIMUM_MESSAGE_RESULT_ALERT, new String[]{String.ValueOf(CONST_MAX_SIZE)}));
                ApexPages.addMessage(msg);
            }
            //check less than 10000 records but over the setup from administrator.
            else if (this.allMessageRecord >= maxDataItems)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, String.format(Label.MB_MAXIMUM_MESSAGE_RESULT_ALERT, new String[]{Label.MB_MAXIMUM_MESSAGE_ITEM}));
                ApexPages.addMessage(msg);
            }
            return result;
        }
        catch (Exception e)
        {
            System.debug(e.getMessage());
        }
        return null;
    }
    
    /**
    *  @ author: CuongPQ.
    *  @ date: 16 Mar 2016.
    *  @ description: convert the data string to number.
    *  @ param1 pLabel: the label set up by administrator.
    */
    private Integer convertLabelIntoInteger(String pLabel)
    {
            Integer data = -1;
            try
            {
                data = Integer.valueOf(pLabel);
            }
            catch (Exception e)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.MB_Error_Configure_Message);
                ApexPages.addMessage(msg);
                System.debug('[MB_MessageList_Ctr] - convertLabelIntoInteger: ' + e.getMessage()); 
            }
            
            return data ;
    }
}